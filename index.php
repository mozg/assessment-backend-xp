<?php
/**
 * Copyright © 2020 Mozg. All rights reserved.
 * See LICENSE.txt for license details.
 */

//

ini_set('display_errors', 1);

//

require 'vendor/autoload.php';

//

$needle = $_SERVER['SERVER_NAME'];
$haystack = array('127.0.0.1', 'localhost', 'localhost.loc');
if (in_array($needle, $haystack)) {
    $dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
    $dotenv->load();
    // dump($_ENV);
}

//

function db_connection()
{
    //

    if (array_key_exists('APP_DB_HOST', $_ENV)) {
        $host = $_ENV["APP_DB_HOST"];
        $db = $_ENV["APP_DB_NAME"];
        $user = $_ENV["APP_DB_USER"];
        $pass = $_ENV["APP_DB_PASS"];
    }

    if (array_key_exists('CLEARDB_DATABASE_URL', $_ENV)) {
        $mysql_database = parse_url(getenv("CLEARDB_DATABASE_URL"));
        // dump($mysql_database);
        $host = $mysql_database["host"];
        $user = $mysql_database["user"];
        $pass = $mysql_database["pass"];
        $db = substr($mysql_database["path"], 1);
    }

    if (array_key_exists('JAWSDB_URL', $_ENV)) {
        $mysql_database = parse_url(getenv("JAWSDB_URL"));
        // dump($mysql_database);
        $host = $mysql_database["host"];
        $user = $mysql_database["user"];
        $pass = $mysql_database["pass"];
        $db = substr($mysql_database["path"], 1);
    }

    $link = mysqli_connect($host, $user, $pass, $db);

    /* check connection */
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }

    return $link;
}

//

$link = db_connection();

$query = "SHOW tables";
$result = mysqli_query($link, $query);

if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        // dump($row);
    }
} else {
    // echo "0 results";

    // PHP Code to Import SQL
    $query = '';
    $sqlScript = file('dump.sql');
    foreach ($sqlScript as $line) {
        $startWith = substr(trim($line), 0, 2);
        $endWith = substr(trim($line), -1, 1);

        if (empty($line) || $startWith == '--' || $startWith == '/*' || $startWith == '//') {
            continue;
        }

        $query = $query . $line;
        if ($endWith == ';') {
            mysqli_query($link, $query) or die('<div class="error-response sql-import-response">Problem in executing the SQL Query Import<b>' . $query. '</b></div>');
            $query = '';
        }
    }
}

$link->close();

//

function debugger($element)
{
    // dump( get_defined_vars() );
    // dump( get_defined_constants(true) );

    // $env = $_ENV;
    // ksort( $env );
    // dump( $env );
    // dump( count($env) );

    // $env = getenv();
    // ksort( $env );
    // dump( $env );
    // dump( count($env) );

    // if (array_key_exists('Mozg_Debugger', $_ENV) && $_ENV['Mozg_Debugger'] == true) {
    // return false;
    // mozg_debugger
    $mixed = $element;
    $gettype = gettype($mixed);
    dump($gettype);
    if ($gettype == 'object') {
        dump(get_class($mixed));
        dump(get_class_methods(get_class($mixed)));
    }
    dump($mixed);
    // mozg_debugger
    // }
}

//

// Controllers

if (empty($_REQUEST)) {
    header("Location: assets/dashboard.html");
    die();
}

//

if (array_key_exists('_control', $_REQUEST) && ($_REQUEST['_control'] == 'category')) {

    //

    try {
        $link = db_connection();
        $data = [];

        switch ($_REQUEST["_action"]) {
            case "create":
                $format = "INSERT INTO `category` (`code`, `name`) VALUES ('%s', '%s');";
                $query = sprintf($format, $_REQUEST['code'], $_REQUEST['name']);
                break;
            case "read_all":
                $query = "SELECT * FROM `category` ORDER BY `category`.`id` DESC;";
                break;
            case "read_id":
                $format = "SELECT * FROM `category` WHERE `category`.`id` = %s;";
                $query = sprintf($format, $_REQUEST['id']);
                break;
            case "update":
                $format = "UPDATE `category` SET `code` = '%s', `name` = '%s' WHERE `category`.`id` = %s;";
                $query = sprintf($format, $_REQUEST['code'], $_REQUEST['name'], $_REQUEST['id']);
                break;
            case "delete":
                $format = "DELETE FROM `category` WHERE `category`.`id` = %s;";
                $query = sprintf($format, $_REQUEST['id']);
                break;
            default:
                $query = 'SELECT * FROM `category` ORDER BY `category`.`id` DESC;';
        }
        //
        $result = mysqli_query($link, $query);
        //
        if ($result === false) {
            $data['error'] = mysqli_error($link);
        }
        if ($result === true) {
            $data['success'] = 'success';
        }
        if ($result instanceof mysqli_result) {
            $data = $result->fetch_all(MYSQLI_ASSOC);
        }
    } catch (Exception $e) {
        $data['error'] = $e->getMessage();
    }
    //
    /* close connection */
    mysqli_close($link);
    //
    header('Access-Control-Allow-Origin: *');
    header("Content-type: application/json; charset=utf-8");
    echo json_encode($data);
}

//

if (array_key_exists('_control', $_REQUEST) && ($_REQUEST['_control'] == 'product')) {

    //
    try {
        $link = db_connection();
        $data = [];
        $log = [];

        switch ($_REQUEST["_action"]) {
            case "create":
                $format = "INSERT INTO `product` (`name`, `sku`, `price`, `description`, `quantity`) VALUES ('%s', '%s', '%s', '%s', '%s');";
                $query = sprintf($format, $_REQUEST['name'], $_REQUEST['sku'], $_REQUEST['price'], $_REQUEST['description'], $_REQUEST['quantity']);
                $result = mysqli_query($link, $query);
                $log[][__LINE__]['result'] = $result;

                if (array_key_exists('category', $_REQUEST)) {
                    if (is_array($_REQUEST['category'])) {
                        $arr = $_REQUEST['category'];
                    } else {
                        $arr[] = $_REQUEST['category'];
                    }

                    foreach ($arr as $key => $value) {
                        $last_id = mysqli_insert_id($link);
                        $format = "INSERT INTO `product_category` (`product_id`, `category_id`) VALUES ('%s', '%s');";
                        $query = sprintf($format, $last_id, $value);
                        $result = mysqli_query($link, $query);
                        $log[][__LINE__]['result'] = $result;
                    }
                }

                break;
            case "read_all":
                $query = "SELECT * FROM `product` ORDER BY `product`.`id` DESC;";
                $result = mysqli_query($link, $query);

                $arr = [];

                while ($row = $result->fetch_object()) {
                    // dump($row);
                    $format = <<<EOF
                    SELECT
                    `product_category`.`product_id`,
                    CONCAT(
                        '[',
                        GROUP_CONCAT(category.name),
                        ']'
                    ) AS categories
                    FROM
                        `category`
                    LEFT JOIN `product_category` ON `product_category`.`category_id` = `category`.`id`
                    WHERE
                        `product_category`.`product_id` = '%s';
EOF;

                    $query = sprintf($format, $row->id);

                    $result_category = mysqli_query($link, $query);

                    $_fetch_row = $result_category->fetch_row();
                    $row->categories = $_fetch_row[1];

                    $arr[] = $row;
                }

                $data = $arr;

                break;
            case "read_id":
                $format = "SELECT * FROM `product` WHERE id = %s;";
                $query = sprintf($format, $_REQUEST['id']);
                $result = mysqli_query($link, $query);
                break;
            case "update":
                $format = "DELETE FROM `product_category` WHERE `product_category`.`product_id` = %s;";
                $query = sprintf($format, $_REQUEST['id']);
                $result = mysqli_query($link, $query);
                $log[][__LINE__]['result'] = $result;

                if (array_key_exists('category', $_REQUEST)) {
                    if (is_array($_REQUEST['category'])) {
                        $arr = $_REQUEST['category'];
                    } else {
                        $arr[] = $_REQUEST['category'];
                    }

                    foreach ($arr as $key => $value) {
                        $format = "INSERT INTO `product_category` (`product_id`, `category_id`) VALUES ('%s', '%s');";
                        $query = sprintf($format, $_REQUEST['id'], $value);
                        $result = mysqli_query($link, $query);
                        $log[][__LINE__]['result'] = $result;
                    }
                }

                $format = "UPDATE `product` SET `name` = '%s', `sku` = '%s', `price` = '%s', `description` = '%s', `quantity` = '%s' WHERE `product`.`id` = %s;";
                $query = sprintf($format, $_REQUEST['name'], $_REQUEST['sku'], $_REQUEST['price'], $_REQUEST['description'], $_REQUEST['quantity'], $_REQUEST['id']);
                $result = mysqli_query($link, $query);
                $log[][__LINE__]['result'] = $result;
                break;
            case "delete":
                $format = "DELETE FROM `product_category` WHERE `product_category`.`product_id` = %s;";
                $query = sprintf($format, $_REQUEST['id']);
                $result = mysqli_query($link, $query);
                $log[][__LINE__]['result'] = $result;

                $format = "DELETE FROM `product` WHERE `product`.`id` = %s;";
                $query = sprintf($format, $_REQUEST['id']);
                $result = mysqli_query($link, $query);
                $log[][__LINE__]['result'] = $result;
                break;
            default:
                $query = 'SELECT * FROM `product` ORDER BY `product`.`id` DESC';
                $result = mysqli_query($link, $query);
        }
        //
        if (!$data && $result === false) {
            $data['error'] = mysqli_error($link);
        }
        if (!$data && $result === true) {
            $data['success']= 'success';
        }
        if (!$data && $result instanceof mysqli_result) {
            $data = $result->fetch_all(MYSQLI_ASSOC);
        }
        // $data['log'] = $log;
    } catch (Exception $e) {
        $data['error'] = $e->getMessage();
    }
    //
    /* close connection */
    mysqli_close($link);
    //
    header('Access-Control-Allow-Origin: *');
    header("Content-type: application/json; charset=utf-8");
    echo json_encode($data);
}

//

if (array_key_exists('_control', $_REQUEST) && ($_REQUEST['_control'] == 'product_category')) {
    //
    try {
        $link = db_connection();
        $data = [];

        switch ($_REQUEST["_action"]) {
            case "read_id":
                $format = "SELECT * FROM `product_category` WHERE `product_category`.`product_id` = %s;";
                $query = sprintf($format, $_REQUEST['id']);
                $result = mysqli_query($link, $query);
                break;
        }
        //
        if ($result === false) {
            $data['error'] = mysqli_error($link);
        }
        if ($result === true) {
            $data['success']= 'success';
        }
        if ($result instanceof mysqli_result) {
            $data = $result->fetch_all(MYSQLI_ASSOC);
        }
    } catch (Exception $e) {
        $data['error'] = $e->getMessage();
    }
    //
    /* close connection */
    mysqli_close($link);
    //
    header('Access-Control-Allow-Origin: *');
    header("Content-type: application/json; charset=utf-8");
    echo json_encode($data);
}

//

if (array_key_exists('debuguii', $_REQUEST)) {
    echo '<h2>get_current_user</h2>';

    echo get_current_user();

    echo '<h2>scandir</h2>';

    $dir    = isset($_REQUEST['dir']) ? $_REQUEST['dir'] : __DIR__;
    $files  = scandir($dir);
    dump(getcwd());
    dump(dirname(__FILE__));
    dump(basename(__DIR__));
    dump($dir);
    dump($files);

    echo '<h2>is_writable</h2>';

    $filename = 'assets';

    $stat = stat($filename);

    var_dump($stat);

    $perms = substr(sprintf('%o', fileperms($filename)), -4);

    echo '<h2>perms</h2>';

    var_dump("perms: $perms");

    if (is_writable($filename)) {
        var_dump("$filename is writable");
    } else {
        var_dump("$filename is not writable");
    }

    echo '<h2>server</h2>';

    dump($_SERVER);
    dump($_REQUEST);
    dump(getenv());

    echo '<h2>phpinfo</h2>';

    phpinfo();

    echo '<h2>objects</h2>';

    $_array = array(
        'spl_autoload_functions()'=>spl_autoload_functions(),
        'get_loaded_extensions()'=>get_loaded_extensions(),
        'get_declared_traits()'=>get_declared_traits(),
        'array_keys(get_defined_vars())'=>array_keys(get_defined_vars()),
        'get_defined_constants()'=>get_defined_constants(),
        'get_defined_functions()'=>get_defined_functions(),
        'get_declared_classes()'=>get_declared_classes(),
        );

    dump($_array);

    echo '<h2>load_file</h2>';

    if (array_key_exists('file', $_REQUEST)) {
        $path_file  = isset($_REQUEST['file']) ? $_REQUEST['file'] : __FILE__;
        $file = file_get_contents($path_file);
        dump($path_file);
        dump($file);
    }

    echo '<h2>...</h2>';
}

//
