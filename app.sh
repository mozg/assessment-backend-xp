#!/bin/bash

# Copyright © 2016-2020 Mozg. All rights reserved.

#

function app_postdeploy() {
  echo "app_postdeploy"
}

function app_predestroy() {
  echo "app_predestroy"
}

function app_environments() {
  echo "app_environments"
}

#

function composer_post_update_cmd() {
  echo "composer_post_update_cmd"
}

function composer_post_install_cmd() {
  echo "composer_post_install_cmd"
}

#

${1}
